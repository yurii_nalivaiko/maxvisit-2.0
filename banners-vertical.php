<?php while (have_rows('banners')): the_row(); ?>
<div class="row banners-vertical single-banner">
    <?php $image = get_sub_field('image'); ?>
    <?php $title = get_sub_field('title'); ?>
    <div class="col-sm-6 col-sm-offset-1 image">
        <img class="img-responsive" src="<?= esc_attr($image['url']) ?>"
             alt="<?= esc_attr($title ?: get_the_title()) ?>">
    </div>
    <div class="col-sm-4 embed-code">
        <strong><?=
            $title ? do_shortcode(esc_html($title)) : sprintf('%dx%d', $image['width'], $image['height'])
      ?></strong>
        <textarea cols="5" rows="5" onclick="this.select()"><?= esc_html(
            apply_filters('mrk/banners/get_code', '', array())
        ) ?></textarea>
        <input class="button button-primary" type="button" 
               value="<?= esc_attr(__('Copy banner code', 'mrk')) ?>">
    </div>
</div>
<?php endwhile;
