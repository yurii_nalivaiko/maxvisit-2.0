<?php namespace Rebel\SuperScript\Themes\MrKortingscode; ?>
<?php get_header(); ?>
<?php the_post(); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-8 nml">
            <div id="categories">
                <?php if (has_post_thumbnail()): ?>
                    <div class="page-thumb" style="background-image: url('<?= get_the_post_thumbnail_url(get_the_ID(), 'large') ?>');"></div>
                <?php endif; ?>
                <div class="cat-padding">
                    <h1 class="mobil-title"><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-4 text-mobile-center sidebar">
            <?php dynamic_sidebar( 'sidebar-1' ); ?>
        </div>
    </div>
</div>
<?php get_footer();
