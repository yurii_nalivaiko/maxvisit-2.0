<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>

<table class="head container">
	<tr>
		<td class="header">
		<?php
		if( $this->has_header_logo() ) {
			$this->header_logo();
		} else {
			echo $this->get_title();
		}
		?>
		</td>
		<td class="shop-info">
			<div class="shop-name"><h3><?php $this->shop_name(); ?></h3></div>
			<div class="shop-address"><?php $this->shop_address(); ?></div>
		</td>
	</tr>
</table>

<?php 
	$items = $this->get_order_items();
	$items_keys = array_keys($items);
	$item_id = $items[$items_keys[0]];
	$item_url = wc_get_order_item_meta( $item_id, 'url');
	$country = WC()->countries->countries[$this->order->get_billing_country()];
	$first_name = $this->order->get_billing_first_name();
	$last_name = $this->order->get_billing_last_name();
	$data = get_post_meta($this->order_id);
?>

<h1>
	<?php echo __('Invoice ID', 'woocommerce-pdf-invoices-packing-slips'); ?> #<?php $this->invoice_number(); ?>

<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>

<table class="order-data-addresses">
	<tr>
		<td class="address billing-address">
			<!-- <h3><?php _e( 'Billing Address:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3> -->
			<?php do_action( 'wpo_wcpdf_before_billing_address', $this->type, $this->order ); ?>
			<?php $this->billing_address(); ?>
			<?php do_action( 'wpo_wcpdf_after_billing_address', $this->type, $this->order ); ?>
			<?php if ( isset($this->settings['display_email']) ) { ?>
			<div class="billing-email"><?php $this->billing_email(); ?></div>
			<?php } ?>
			<?php if ( isset($this->settings['display_phone']) ) { ?>
			<div class="billing-phone"><?php $this->billing_phone(); ?></div>
			<?php } ?>
		</td>
		<td class="address shipping-address">
			<?php if ( isset($this->settings['display_shipping_address']) && $this->ships_to_different_address()) { ?>
			<h3><?php _e( 'Ship To:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
			<?php do_action( 'wpo_wcpdf_before_shipping_address', $this->type, $this->order ); ?>
			<?php $this->shipping_address(); ?>
			<?php do_action( 'wpo_wcpdf_after_shipping_address', $this->type, $this->order ); ?>
			<?php } ?>
		</td>
		<td class="order-data">
			<table>
				<?php do_action( 'wpo_wcpdf_before_order_data', $this->type, $this->order ); ?>
				<?php if ( isset($this->settings['display_number']) ) { ?>
				<tr class="invoice-number">
					<th><?php _e( 'Invoice Number:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->invoice_number(); ?></td>
				</tr>
				<?php } ?>
				<?php if ( isset($this->settings['display_date']) ) { ?>
				<tr class="invoice-date">
					<th><?php _e( 'Invoice Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->invoice_date(); ?></td>
				</tr>
				<?php } ?>
				<tr class="order-number">
					<th><?php _e( 'Order Number:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->order_number(); ?></td>
				</tr>
				<tr class="order-date">
					<th><?php _e( 'Order Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->order_date(); ?></td>
				</tr>
				<tr class="payment-method">
					<th><?php _e( 'Payment Method:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $payment_method = $this->payment_method(); ?></td>
				</tr>

				<?php  if ($this->order->get_payment_method() == 'paypal'): ?>
					<?php if (isset($data['Payer PayPal address'][0])): ?>
						<tr class="paypal-id">
							<th><?php _e( 'PayPal ID:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
							<td><?php echo $data['Payer PayPal address'][0]; ?></td>
						</tr>
					<?php endif ?>
					<?php if (isset($data['_transaction_id'][0])): ?>
						<tr class="paypal-transaction-id">
							<th><?php _e( 'Transaction ID:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
							<td><?php echo $data['_transaction_id'][0]; ?></td>
						</tr>
					<?php endif ?>
				<?php endif ?>

				<?php do_action( 'wpo_wcpdf_after_order_data', $this->type, $this->order ); ?>
			</table>			
		</td>
	</tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $this->type, $this->order ); ?>

<table class="order-details">
	<thead>
		<tr>
			<th class="product"><?php _e('Product', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="quantity"><?php _e('Quantity', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="price"><?php _e('Price', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>
		<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $this->type, $this->order, $item_id ); ?>">
			<td class="product">
				<?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
				<span class="item-name"><?php echo $item['name']; ?></span>
				<?php do_action( 'wpo_wcpdf_before_item_meta', $this->type, $item, $this->order  ); ?>
				<span class="item-meta"><?php echo $item['meta']; ?></span>
				<dl class="meta">
					<?php $description_label = __( 'SKU', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
					<?php if( !empty( $item['sku'] ) ) : ?><dt class="sku"><?php _e( 'SKU:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="sku"><?php echo $item['sku']; ?></dd><?php endif; ?>
					<?php if( !empty( $item['weight'] ) ) : ?><dt class="weight"><?php _e( 'Weight:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="weight"><?php echo $item['weight']; ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?>
				</dl>
				<?php do_action( 'wpo_wcpdf_after_item_meta', $this->type, $item, $this->order  ); ?>
			</td>
			<td class="quantity"><?php echo $item['quantity']; ?></td>
			<td class="price"><?php echo $item['order_price']; ?></td>
		</tr>
		<?php endforeach; endif; ?>
	</tbody>
	<tfoot>
		<tr class="no-borders">
 			<td class="no-borders" colspan="3">
				<div class="customer-notes">
					<?php do_action( 'wpo_wcpdf_before_customer_notes', $this->type, $this->order ); ?>
					<?php if ( $this->get_shipping_notes() ) : ?>
						<h3><?php _e( 'Customer Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
						<?php $this->shipping_notes(); ?>
					<?php endif; ?>
					<?php do_action( 'wpo_wcpdf_after_customer_notes', $this->type, $this->order ); ?>
				</div>				
			</td>
		</tr>
		<tr class="no-borders">
			<td class="no-borders" >
				<table class="totals">
					<tfoot>
						<?php foreach( $this->get_woocommerce_totals() as $key => $total ) : ?>
							<tr class="<?php echo $key; ?>">
								<td class="no-borders"></td>
								<th class="description"><?php echo $total['label']; ?></th>
								<td class="price"><span class="totals-price"><?php echo $total['value']; ?></span></td>
							</tr>
							<?php 
								if ($key == 'cart_subtotal' && count($this->get_woocommerce_totals()) < 4): 
									$currency_code = $order->get_currency();
							?>
								<tr class="vat">
									<td class="no-borders"></td>
									<th class="description"><?php _e('VAT', 'woocommerce-pdf-invoices-packing-slips'); ?></th>
									<td class="price"><span class="totals-price"><?php echo '0 '.$currency_code; ?></span></td>
								</tr>
							<?php endif ?>
						<?php endforeach; ?>
					</tfoot>
				</table>
			</td>
		</tr>
	</tfoot>
</table>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>
<?php 
	if (function_exists('WooCommerce_EU_VAT_Compliance')) {
		
		$vat_region_countries = WooCommerce_EU_VAT_Compliance()->get_vat_countries();
		$total_tax = $this->order->get_total_tax();
		$taxes = $this->order->get_meta('vat_compliance_country_info');
		$country_code = $taxes['taxable_address'][0];
		$valid_eu_vat_number = $this->order->get_meta('Valid EU VAT Number');

		$text = '';
		if (!in_array($country_code, $vat_region_countries) && empty($total_tax)) {
			$text = get_option('woocommerce_eu_vat_compliance_pdf_footer_b2b_noneu');
		} elseif ($valid_eu_vat_number && $country_code != 'NL') {
			$text = get_option('woocommerce_eu_vat_compliance_pdf_footer_b2b');
		}
		echo $text;

	}
?>

<h2 style="font-size: 16px;">
	<?php echo __('Invoice ID', 'woocommerce-pdf-invoices-packing-slips'); ?> #<?php $this->invoice_number(); ?> - <?php echo $item_url . ' ' . $first_name . ' ' . $last_name . ' - ' .  $country . ''; ?> 
</h2>

<?php if ( $this->get_footer() ): ?>
<div id="footer">
	<?php //$this->footer(); ?>
	<?php
		$wpo_wcpdf_settings_general = get_option('wpo_wcpdf_settings_general');
		if (isset($wpo_wcpdf_settings_general['footer']['default'])) {
			echo $wpo_wcpdf_settings_general['footer']['default'];
		}
	?>
</div><!-- #letter-footer -->
<?php endif; ?>
<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>
