<?php
/**
 * The template for displaying product widget entries.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-widget-product.php.
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.5
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

if ( ! is_a( $product, 'WC_Product' ) ) {
	return;
}

$product_id = $product->get_id();
$ppom = new PPOM_Meta( $product_id );
$the_meta = (array) json_decode($ppom->ppom_settings->the_meta);

$user = wp_get_current_user();
$roles = ( array ) $user->roles;
$reseller_discount_perc = get_field('reseller_discount', 'option');
$slug = $product->get_slug();
$allowed_packages = get_field('allowed_packages', 'option');

if ( $reseller_discount_perc && in_array('reseller', $roles) && in_array($product_id, $allowed_packages)) {
	$arr_key = 0;
} elseif (in_array($product_id, $allowed_packages)) {
	$arr_key = 1;
} else {
	$arr_key = 0;
}

foreach ($the_meta as $key => $value) {
	if ($key == '1') {
		$volume_price = $value->options[$arr_key]->price;
		break;
	}
}

if ($reseller_discount_perc && in_array('reseller', $roles) && in_array($product_id, $allowed_packages)) {
	$volume_price = $volume_price - ($volume_price * ($reseller_discount_perc / 100));
	$volume_price = round($volume_price, 2);
}

$price = wc_price($volume_price); 
$faq_url = get_post_meta($product_id, 'faq_url', true);

if ($product->is_featured()) {
	$class = 'popular';
	$label = '<span class="label">'.__('Popular', 'shoptimizer').'</span>';
} else {
	$class = $label = '';
}

$cur_user_id = get_current_user_id();
$extra_packages_func = get_field('extra_packages_func', 'option');
$extra_packages = get_field('extra_packages', 'option');
$is_it_trusted_user = get_field('is_it_trusted_user', 'user_'.$cur_user_id);

if ( !$extra_packages_func || !in_array($product_id, $extra_packages) ) {
?>
<li class="<?php echo $class; ?>">
	<?php echo $label; ?>

	<div class="row">
		<div class="column left col-75">
			<div class="product-head">
				<?php echo $product->get_image();?>
				<span class="product-title"><?php echo wp_kses_post( $product->get_name() ); ?></span>
				<i class="info" title="<?php _e('Max Visits Product', 'shoptimizer'); ?>"></i>
			</div>
			<div class="product-content">
				<?php echo $product->get_short_description(); ?>
			</div>
		</div>
		<div class="column right col-25">
			<div class="product-head">
				<span class="product-list-price"><?php _e('from', 'shoptimizer'); ?> <?php echo $price; ?></span>
			</div>
			<div class="product-content">
				<a href="<?php echo esc_url( $product->get_permalink() ); ?>" class="order-button">
					<?php _e('Order Now', 'shoptimizer'); ?> »
				</a>
				<?php if ($faq_url): ?>
					<a href="<?php echo esc_url($faq_url); ?>" class="order-faq"><?php _e('Read More', 'shoptimizer'); ?></a>
				<?php endif ?>
			</div>
		</div>
	</div>

</li>
<?php } ?>
