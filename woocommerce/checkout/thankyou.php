<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="woocommerce-order">

	<?php if ( $order ) : ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<?php 
				if ( $order->get_payment_method() == 'bacs' ): 
					$bacs_instructions = get_option('woocommerce_bacs_settings')["instructions"];
			?>

				<div class="success-top-text">
					<h1 class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received text-center"><?php echo apply_filters('woocommerce_thankyou_order_received_text', __('Congratulations!', 'woocommerce'), $order); ?></h1>

					<input type="hidden" id="order_totals" value="<?php echo $order->total;?>">
	                <input type="hidden" id="currency_code" value="<?php echo $order->get_currency();?>">

	                <script>
	                    jQuery(document).ready(function ($) {
	                        var purchase = $('#order_totals').val();
	                        var currency_code = $('#currency_code').val();
	                        fbq('track', 'Purchase', { value: purchase, currency: currency_code});
	                    });
	                </script>

	                <p><?php printf(__('You have bought Traffic plan for %s', 'woocommerce'), $order->get_formatted_order_total()); ?></p>
	                <p><?php printf(__('Your Order Number: %s', 'woocommerce'), $order->get_order_number()); ?></p>

	            </div>

				<div class="thank-you-bacs">
			        <div class="thank-you-bacs-header">
			            <p><?php echo wp_kses_post(wpautop(wptexturize(wp_kses_post($bacs_instructions)))); ?></p>
			        </div>


			        <div class="container-fluid">
			            <div class="row">
			                <div class="thank-you-bacs-body">

			                    <?php

			                    	$bacs_info = get_option('woocommerce_bacs_accounts');
			                    	$order_currency = get_post_meta($order->get_id(), '_order_currency', true);
			                    	$str_out = '';

				                    foreach ($bacs_info as $account) {

				                    	$billing_company = $order->get_billing_company();

			                            if (empty($billing_company)) {

			                                $new_order_currency = $order_currency;
			                                $info_method = 'TransferWise';

			                            } else {

			                                $eu_countries = ['AT','BE','BG','CY','CZ','DK','EE','FI','FR','DE','GR','HU','IE','IT','LV','LT','LU','MT','NL','PL','PT','RO','SK','SI','ES','SE','GB','RD', 'UK'];
			                                $current_billing_country = $order->get_billing_country();

			                                if ($current_billing_country == 'US') {
			                                    $new_order_currency = 'USD';
			                                    $info_method = 'Payoneer';
			                                } elseif ( in_array($current_billing_country, $eu_countries) ) {
			                                    $new_order_currency = 'EUR';
			                                    $info_method = 'Payoneer';
			                                } else {
			                                    $new_order_currency = $order_currency;
			                                    $info_method = 'TransferWise';
			                                }

			                            }

			                            if (($new_order_currency == 'USD' && strpos($account['account_name'], 'USD') !== false) || ($new_order_currency == 'EUR' && strpos($account['account_name'], 'EUR') !== false)) {

			                                if ( strpos($account['account_name'], $info_method) !== false ) {
			                                    $str_out .= '</div><div class="col-xs-12 col-sm-6 col-md-6 pl-45">';

			                                    $str_out .= '<h3 class="left">' . $account['account_name'] . '</h3>';

			                                    $str_out .= '<p class="bank-name left">' . str_replace(array( '|', '*b/', '*b' ) , array('<br>', '</b>', '<b>'), $account['bank_name']) . '</p>';
			                                    $str_out .= '<p class="bank-name left">* ' . sprintf(__('Fill this inside description field of payment: <b>Order ID %s</b>', 'woocommerce'), $order->get_order_number()) . '</p>';
			                                }
			                            }

			                        }

			                        $str_out .= '</div>';
                    				echo $str_out;
				                ?>

				            </div>
			            </div>
			        </div>
			    </div>
			
			<?php else: ?>
			
				<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you, your order has been succesfully received.', 'woocommerce' ), $order ); ?></p>

				<p class="woocommerce-notice center"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'We will be working hard on fullfilling your order asap. We will e-mail you as soon as your campaign is started.', 'woocommerce' ), $order ); ?></p>

				<p class="woocommerce-notice center italic"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'MaxVisits: Cost Effective Legit Web Traffic. Since 2011', 'woocommerce' ), $order ); ?></p>

				<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

					<li class="woocommerce-order-overview__order order">
						<?php _e( 'Order number:', 'woocommerce' ); ?>
						<strong><?php echo $order->get_order_number(); ?></strong>
					</li>

					<li class="woocommerce-order-overview__date date">
						<?php _e( 'Date:', 'woocommerce' ); ?>
						<strong><?php echo wc_format_datetime( $order->get_date_created() ); ?></strong>
					</li>

					<?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
						<li class="woocommerce-order-overview__email email">
							<?php _e( 'Email:', 'woocommerce' ); ?>
							<strong><?php echo $order->get_billing_email(); ?></strong>
						</li>
					<?php endif; ?>

					<li class="woocommerce-order-overview__total total">
						<?php _e( 'Total:', 'woocommerce' ); ?>
						<strong><?php echo $order->get_formatted_order_total(); ?></strong>
					</li>

					<?php if ( $order->get_payment_method_title() ) : ?>
						<li class="woocommerce-order-overview__payment-method method">
							<?php _e( 'Payment method:', 'woocommerce' ); ?>
							<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
						</li>
					<?php endif; ?>

				</ul>

			<?php endif ?>

		<?php endif; ?>
		<?php if ( $order->get_payment_method() !== 'bacs' ): ?>
			<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
			<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
		<?php endif; ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

	<?php endif; ?>

</div>
<?php if ( $order ) : ?>
	<?php if ( !$order->has_status( 'failed' ) ) : ?>
		<?php 
		  $first_name = get_post_meta($order->get_id(),'_billing_first_name', true);
		  $last_name = get_post_meta($order->get_id(),'_billing_last_name', true);
		  $name  = $first_name . ' ' . $last_name;
		  $order_items = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
		?>
		<script>
			dataLayer.push({
				'event':'ecomm_event',
			    'transactionId': '<?php echo $order->get_order_number(); ?>',
			    'transactionAffiliation': '<?php echo $name; ?>',
			    'transactionTotal': '<?php echo $order->get_total(); ?>',
			    'transactionTax': '<?php echo $order->get_total_tax(); ?>' ,
			    'transactionShipping':'0',
			    'transactionProducts': [
			    	<?php foreach ( $order_items as $item_id => $item ): 
			    		$product = $item->get_product();
			    		$sku = (empty($product->get_sku()))? strtolower(str_replace(' ', '_', $product->get_name())) : $product->get_sku();
			    		$product_cats = wp_get_post_terms($product->get_id(), 'product_cat');
			    	?>
						{
					        'sku': '<?php echo $sku; ?>',
					        'name': '<?php echo $product->get_name(); ?>',
					        'category': '<?php echo $product_cats[0]->name; ?>',
					        'price': '<?php echo $item->get_subtotal(); ?>',
					        'quantity': '1'
					    }
					<?php endforeach; ?>
			    ]
			});
		</script>
		<script> var success_order_total = '<?php echo $order->get_total(); ?>'; </script>
	<?php endif; ?>
<?php endif; ?>