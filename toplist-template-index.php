<?php

/**
 * Template Name: Index
 * Template Post Type: toplist
 */

namespace Rebel\SuperScript\Themes\MrKortingscode;

get_header();
the_post();

?>
<div class="toplist-index-template container-fluid">
    <?php if (has_post_thumbnail()): ?>
    <div class="mrk-page-thumb row">
        <div class="col-sm-12">
            <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
        </div>
    </div>
    <?php endif; ?>
    <div class="row">
        <div class="mrk-sites col-sm-8">
            <h1 class="mrk-header"><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div>
        <div class="col-sm-4">
            <?php inc_template_part('sidebar', 'newsletter'); ?>
            <?php inc_template_part('sidebar', 'popular-shops'); ?>

        </div>
    </div>
</div>
<script src="//cdn.jsdelivr.net/clipboard.js/1.6.1/clipboard.min.js"></script>
<script>
    jQuery(function ($) {
        $('.mrk-index').on('click', 'a', function (e) {
            var href    = $(this).attr('href');
            var website = $('.mrk-toplist-website a[href="' + href + '"]').closest('.mrk-toplist-website');
            var offset  = $(website).offset();
            
            $('html, body').animate({ 'scrollTop': offset.top - 50 });
            e.preventDefault();
        });
        
        $('.mrk-code').on('click', 'textarea', function () {
            this.select();
        });
        
        $('.mrk-code').on('change', 'select', function () {
            var textarea = $(this).closest('.mrk-code').find('textarea');
            var code     = $(textarea).val();
            var width    = $(this).val();
            
            $(textarea).val(code.replace(/width: \d+px;/, 'width: ' + width + 'px;'));
        });
        
        new Clipboard('body.single-toplist .mrk-code input', {
            target: function(trigger) {
                return $(trigger).closest('.mrk-code').find('textarea').get(0);
            }
        });
    });
</script>
<?php get_footer();
