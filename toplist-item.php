<?php namespace Rebel\SuperScript\Themes\MrKortingscode; ?>
<div class="toplist-website">
    <h4 class="site-name"><?php do_action('mrk/toplists/print_website_name'); ?></h4>
    <div class="site-logo">
        <div class="site-logo-wrap">
            <?php do_action('mrk/toplists/print_website_logo'); ?>
            <?php do_action('mrk/toplists/print_logo_badge'); ?>
        </div>
    </div>
    <table class="site-ratings">
        <tbody>
            <tr>
                <th><?= _x('Originality', 'toplist website', 'mrk') ?></th>
                <td><?php do_action('mrk/toplists/print_website_rating', 'originality'); ?></td>
            </tr>
            <tr>
                <th><?= _x('Design', 'toplist website', 'mrk') ?></th>
                <td><?php do_action('mrk/toplists/print_website_rating', 'design'); ?></td>
            </tr>
            <tr>
                <th><?= _x('Social Media', 'toplist website', 'mrk') ?></th>
                <td><?php do_action('mrk/toplists/print_website_rating', 'social_media'); ?></td>
            </tr>
        </tbody>
    </table>
    <div class="site-desc">
        <?php the_sub_field('description'); ?>
    </div>
</div>