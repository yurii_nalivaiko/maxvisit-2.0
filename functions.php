<?php

/**
 * Include files
 */

require_once('inc/banners.php');
require_once('inc/toplists.php');


/**
 * Loads parent and child theme scripts.
 */
function shoptimizer_child_enqueue_scripts() {
	$parent_style    = 'shoptimizer-style';
	$parent_base_dir = 'shoptimizer';

	wp_enqueue_script( 'shoptimizer-jquery', get_stylesheet_directory_uri() . '/assets/js/jquery.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'shoptimizer-main-head', get_stylesheet_directory_uri() . '/assets/js/main-head.js', array( 'jquery' ), '20161207', true );

	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css', array(), wp_get_theme( $parent_base_dir ) ? wp_get_theme( $parent_base_dir )->get( 'Version' ) : '' );
	wp_enqueue_style( 'shoptimizer-child-ibm', 'https://fonts.googleapis.com/css?family=IBM+Plex+Sans:400,600,700&display=optional' );
	wp_enqueue_style( 'shoptimizer-child-style', get_stylesheet_directory_uri() . '/style.css', array( $parent_style ), wp_get_theme()->get('Version') );
	wp_enqueue_style( 'bootstrap-child-style', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css', array( $parent_style ), wp_get_theme()->get('Version') );
    wp_enqueue_style( 'elemntor-child-style', get_stylesheet_directory_uri() . '/assets/css/elementor.css', array( $parent_style ), wp_get_theme()->get('Version') );

    wp_enqueue_script( 'shoptimizer-main-child', get_stylesheet_directory_uri() . '/assets/js/main-child.js', array( 'jquery' ), '20161207', true );
}

add_action( 'wp_enqueue_scripts', 'shoptimizer_child_enqueue_scripts' );


function woo_init() {
	//Remove
	remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
	remove_action( 'shoptimizer_single_post', 'shoptimizer_post_meta', 40 );
	// Single
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20); 
	remove_action( 'woocommerce_before_add_to_cart_button', 'ppom_woocommerce_show_fields', 15);

	// Add
	add_action( 'woocommerce_archive_description', 'woo_category_links', 0);
	add_action( 'woocommerce_archive_description', 'woo_title', 5);

	// Single
	add_action( 'woocommerce_single_product_summary', 'add_single_description', 20);
	add_action( 'woocommerce_before_add_to_cart_button', 'ppom_woocommerce_show_fields_custom', 15);
	add_action( 'woocommerce_single_product_summary', 'continue_shopping_button', 35);
	add_action( 'woocommerce_single_product_summary', 'single_shop_sponsor', 36);
	add_action( 'woocommerce_single_product_summary', 'single_shop_reseller_discount', 25);
	add_action( 'woocommerce_after_add_to_cart_quantity', 'single_shop_reseller_note', 0);

	if ( is_checkout() ) {
		add_action( 'shoptimizer_topbar', 'shoptimizer_top_bar', 15 );
		add_action( 'shoptimizer_before_site', 'shoptimizer_top_bar', 15 );
	}

	remove_action( 'woocommerce_single_product_summary', 'shoptimizer_call_back_feature', 80 );
}
add_action('init', 'woo_init');

function shoptimizer_minimal_checkout_custom() {

	if ( class_exists( 'WooCommerce' ) ) {
		if ( is_checkout() ) {
			add_action( 'shoptimizer_topbar', 'shoptimizer_top_bar', 15 );
			add_action( 'shoptimizer_footer', 'shoptimizer_footer_widgets', 25 );
			add_action( 'shoptimizer_footer', 'shoptimizer_footer_copyright', 35 );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'shoptimizer_minimal_checkout_custom' );


function woo_category_links() {

	$category = get_queried_object();
	$current_cat =  isset($category->term_id) ? $category->term_id : 0;
	$categories = get_categories( array(
			'taxonomy' => 'product_cat',
			'hide_empty'   => 1,
		)
	);
	?>
	<div class="category-links">
		<ul>
			<?php foreach ($categories as $category): 
				$name = $category->cat_name;
				$link = get_category_link($category);
				$class = ($current_cat == $category->cat_ID) ? 'active' : '';
			?>
				<li class="<?php echo $class; ?>"><a href="<?php echo $link; ?>"><?php echo $name; ?></a></li>
			<?php endforeach ?>
		</ul>
	</div>
	<?php
	wp_reset_query();
}

function woo_title() {
	$category = get_queried_object();
	$title =  $category->name;
	echo '<h1 class="woocommerce-products-header__title page-title">'.$title.'</h1>';
}

function custom_remove_all_quantity_fields( $return, $product ) {return true;}
// add_filter( 'woocommerce_is_sold_individually','custom_remove_all_quantity_fields', 10, 2 );

function add_single_description() {
	global $product;
	$description = $product->get_description();
	echo '<div class="product-description">'.$description.'</div>';
}

function ppom_woocommerce_show_fields_custom() {
    
    global $product;
    
    $product_id = ppom_get_product_id( $product ); 
	$ppom		= new PPOM_Meta( $product_id );

	if( ! $ppom->fields ) return '';
	 
	if( ! $ppom->has_unique_datanames ) {
		
		printf(__("<div class='error'>Some of your fields has duplicated datanames, please fix it</div>"), "ppom");
		return;
	}
	
    // Loading all required scripts/css for inputs like datepicker, fileupload etc
    ppom_hooks_load_input_scripts( $product );
    
    
    do_action('ppom_after_scripts_loaded', PPOM() -> productmeta_id, $product);
    
    
    $ppom_html = '<div id="ppom-box-'.esc_attr(PPOM()->productmeta_id).'" class="ppom-wrapper">';
    
    $template_vars = array('ppom_settings'  	=> $ppom->ppom_settings,
    						'product'			=> $product,
    						'ppom_fields_meta'	=> $ppom->fields,
    						'ppom_id'			=> $ppom->meta_id);
    ob_start();

    ppom_load_template_custom ( 'render-fields.php', $template_vars );

    $ppom_html .= ob_get_clean();
    
    // Price container
	$ppom_html .= '<div id="ppom-price-container"></div>';
	
	// Clear fix
	$ppom_html .= '<div style="clear:both"></div>';   // Clear fix
	$ppom_html .= '</div>';   // Ends ppom-wrappper
	
	echo apply_filters('ppom_fields_html', $ppom_html, $product);
}

function ppom_load_template_custom($file_name, $variables=array('')){

	if( is_array($variables))
    extract( $variables );
    
   $file_path =  dirname( __FILE__ ) . '/woocommerce/'.$file_name;
   $file_path = apply_filters('ppom_load_template', $file_path, $file_name, $variables);
    
   if( file_exists($file_path))
   	include ($file_path);
   else
   	die('File not found'.$file_path);
}

function ppom_convert_options_to_key_val_custom($options, $meta, $product) {
	
	if( empty($options) ) return $options;
	
	// Do not change options for cropper
	// if( $meta['type'] == 'cropper' ) return $options;
	
	// ppom_pa($meta);
	
	$ppom_new_option = array();
	foreach($options as $option) {
		
		$the_option = isset($option['option']) ? stripslashes($option['option']) : '';
		
		if( isset($meta['type']) && $meta['type'] == 'imageselect' ) {
			
			$the_option = isset($option['title']) ? stripslashes($option['title']) : '';
		}
		
		if( $the_option != '' ) {
			
			$option = ppom_translation_options($option);
			$option_price_without_tax	= '';
			
			
			$option_percent = '';
			
			$show_price		= isset($meta['show_price']) ? $meta['show_price'] : '';
			$data_name		= isset($meta['data_name']) ? $meta['data_name'] : '';
			
			$option_price	= isset($option['price']) ? $option['price'] : '';
			
			// Currency swithcer
			$product_price		= ppom_get_product_price($product);
			$product_price		= ppom_hooks_convert_price_back($product_price);
			
			// For quantities if default price is set
			if( $meta['type'] == 'quantities' ) {
				$quantities_dp	= isset($meta['default_price']) && $meta['default_price'] != '' ? $meta['default_price'] : $product_price;
				$option_price	= isset($option['price']) && $option['price'] != '' ? $option['price'] : $quantities_dp;
			}
			
			$option_label	= ppom_generate_option_label_custom($option, $option_price, $meta);
			
			// This filter change prices for Currency switcher
			// $option_price	= apply_filters('ppom_option_price', $option_price);
			
			// Price matrix discount
			$discount	= isset($meta['discount']) && $meta['discount'] == 'on' ? true : false;
			$discount_type	= isset($meta['discount_type']) ? $meta['discount_type'] : 'base';
			
			// $show_option_price = apply_filters('ppom_show_option_price', $show_price, $meta);
			if( !empty($option_price) ) {
				
				// $option_price = $option['price'];
				
				// check if price in percent
				if(strpos($option_price,'%') !== false){
					$option_price = ppom_get_amount_after_percentage($product_price, $option_price);
					// check if price is fixed and taxable
					if(isset($meta['onetime']) && $meta['onetime'] == 'on' && isset($meta['onetime_taxable']) && $meta['onetime_taxable'] == 'on') {
						$option_price_without_tax = $option_price;
						$option_price = ppom_get_price_including_tax($option_price, $product);
					}
					
					$option_label	= ppom_generate_option_label_custom($option, $option_price, $meta);
					$option_percent = $option['price'];
				} else {
					
					// check if price is fixed and taxable
					if(isset($meta['onetime']) && $meta['onetime'] == 'on' && isset($meta['onetime_taxable']) && $meta['onetime_taxable'] == 'on') {
						$option_price_without_tax = $option_price;
						$option_price = ppom_get_price_including_tax($option_price, $product);
					}
					$option_label = ppom_generate_option_label_custom($option, $option_price, $meta);
				}
				
			}
			
			
			$option_id = ppom_get_option_id($option, $data_name);
			
			$ppom_new_option[$the_option] = array('label'		=> $option_label,
													'price'		=> apply_filters('ppom_option_price', $option_price),
													'raw_price'	=> $option_price,
													'raw'		=> $the_option,
													'without_tax'=>$option_price_without_tax,
													'percent'	=> $option_percent,
													'data_name' => $data_name,
													'id'		=> $option_id,		// Legacy key fix
													'option_id' => $option_id);
														
			if( $discount ) {
				$ppom_new_option[$the_option]['discount'] = $discount_type;
			}
			
			if( $meta['type'] == 'cropper' ) {
				
				$ppom_new_option[$the_option]['width'] = isset($option['width']) ? $option['width'] : '';
				$ppom_new_option[$the_option]['height'] = isset($option['height']) ? $option['height'] : '';
			}
			
			// Adding weight
			if( isset($option['weight']) ) {
				$ppom_new_option[$the_option]['option_weight'] = $option['weight'];
			}
		}
	}
	
	if( !empty($meta['first_option']) ) {
		
		$fo_labeld = ppom_wpml_translate($meta['first_option'], 'PPOM');
		$first_option = array('' => array('label'=> $fo_labeld, 
										'price'	=> '',
										'raw'	=> '',
										'without_tax' => '')
										);
										
		$ppom_new_option = $first_option + $ppom_new_option;
		// array_unshift( $ppom_new_option, $first_option);
	}
	
	// ppom_pa($ppom_new_option);
	return apply_filters('ppom_options_after_changes', $ppom_new_option, $options, $meta, $product);
}

// Generating option label with price
function ppom_generate_option_label_custom( $option, $price, $meta) {
	
	$the_option = isset($option['option']) ? $option['option'] : '';
	if( isset($meta['type']) && $meta['type'] == 'imageselect' ) {
		$the_option = isset($option['title']) ? $option['title'] : '';
	}
	
	$option_label = !empty($option['label']) ? $option['label'] : $the_option;
	$option_label = stripcslashes($option_label);
	
	if( !empty($price) ) {
		
		$price = apply_filters('woocs_exchange_value', $price);
		$price = strip_tags(wc_price($price));
		$option_label = "{$option_label} <span class='label-price'>{$price}</span>";
	}
	
	return apply_filters('ppom_option_label', $option_label, $option, $meta, $price);
}

// Continue shopping button
function continue_shopping_button() {
	$url = str_replace('.html', '', get_permalink( wc_get_page_id( 'shop' )));
	echo '<a href="'.$url.'" class="continue-shopping">'.__('Continue Shopping', 'shoptimizer').'</a>';
}

// Single shop sponsor code
function single_shop_sponsor() {
	$base_url = get_stylesheet_directory_uri();
	echo '<div class="sponsor-list-container">
			<div class="sponsor-list">
				<img src="'.$base_url.'/assets/img/sponsors/mastercard.png" alt="sponsor">
				<img src="'.$base_url.'/assets/img/sponsors/visa.png" alt="sponsor">
				<img src="'.$base_url.'/assets/img/sponsors/mbg.png" alt="sponsor">
			 </div>
		</div>';
}

// Reseller discount value
function single_shop_reseller_discount() {
	$reseller_discount_perc = reseller_discount_perc();

	if ($reseller_discount_perc) {

		echo '<input type="hidden" value="'.$reseller_discount_perc.'" id="reseller_discount_perc">';

	}
}

// Reseller discount note
function single_shop_reseller_note() {

	$reseller_discount_perc = reseller_discount_perc();

	if ($reseller_discount_perc) {

		echo '<div class="single-reseller-note">' . sprintf( __('Reseller discount (%s%%)', 'shoptimizer'), $reseller_discount_perc) . '</div>';

	}
}

// Reseller discount chek function
function reseller_discount_perc() {
	global $product;
	$product_id = $product->get_id();
	$user = wp_get_current_user();
	$roles = ( array ) $user->roles;
	$reseller_discount_perc = get_field('reseller_discount', 'option');
	$allowed_packages = get_field('allowed_packages', 'option');
	$result = ($reseller_discount_perc && in_array('reseller', $roles) && in_array($product_id, $allowed_packages)) ? $reseller_discount_perc : 0;

	return $result;
}

add_filter( 'wc_add_to_cart_message_html', function( $string, $product_id = 0 ) {
	$start = strpos( $string, '<a href=' ) ?: 0;
	$end = strpos( $string, '</a>', $start ) ?: 0;
	return substr( $string, $end ) ?: $string;
} );

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
	$fields['billing']['billing_phone']['required'] = false;
	$fields['billing']['billing_email']['priority'] = 30;
    $fields['billing']['billing_company']['priority'] = 120;
    return $fields;
}

/**
 * Change a currency symbol
 */
add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

function change_existing_currency_symbol( $currency_symbol, $currency ) {

     $currency_symbol = $currency;

     return $currency_symbol;
}

// Remove slash
add_filter('user_trailingslashit', 'noPage_slash', 66, 2 );
function noPage_slash( $string, $type ){
    global $wp_rewrite;
 
    if( $wp_rewrite->using_permalinks() && $wp_rewrite->use_trailing_slashes == true && $type == 'page'){
        return untrailingslashit( $string );
    }
    return $string;
}
 
// Add .html
add_action('init', 'htmlPage_permalink', -1);
function htmlPage_permalink() {
    global $wp_rewrite;
 		
	if( ! strpos( $wp_rewrite->get_page_permastruct(), '.html') ){
		$wp_rewrite->page_structure = $wp_rewrite->page_structure . '.html';
	}
}

add_filter( 'woocommerce_get_breadcrumb', 'remove_custom_crumb', 20, 2 );
function remove_custom_crumb( $crumbs, $breadcrumb ){

    foreach( $crumbs as $key => $crumb ){
        if( $crumb[0] === __('Clients', 'shoptimizer') ) {
            unset($crumbs[$key]);
        }
    }

    return array_values($crumbs);
}

// add_post_type_support( 'fast_ticket', array('title', 'editor', 'author', 'custom-fields', 'comments') );

function reset_pass_url() {
    $siteURL = get_option('siteurl');
    return "{$siteURL}/my-account.html/lost-password";
}
add_filter( 'lostpassword_url', 'reset_pass_url', 11, 0 );


// Disable the wc-cart-fragments script
add_action( 'wp_print_scripts', 'de_script', 100 );

function de_script() {
    wp_dequeue_script( 'wc-cart-fragments' );

    return true;
}

add_filter('autoptimize_filter_noptimize','my_ao_noptimize',10,0);
function my_ao_noptimize() {
	if (strpos($_SERVER['REQUEST_URI'],'contact')!==false){
		return true;
	} else {
		return false;
	}
}

add_filter ( 'woocommerce_account_menu_items', 'misha_remove_my_account_links' );
function misha_remove_my_account_links( $menu_links ){
 
	unset( $menu_links['downloads'] ); // Disable Downloads
 
	return $menu_links;
 
}

add_filter( 'body_class', 'wc_body_class_custom' );

function wc_body_class_custom( $classes ) {

    if ( is_user_logged_in() ) {
        $classes[] = 'user-login';

        $user = wp_get_current_user();
	 	$roles = ( array ) $user->roles;
		$reseller_discount_perc = get_field('reseller_discount', 'option');

		if ($reseller_discount_perc && in_array('reseller', $roles)) {
			$classes[] = 'reseller-user';
		}
    }

    return $classes;
}

function my_woocommerce_add_error( $error ) {
	$old_message = 'An account is already registered with your email address. Please log in';
	$new_message = sprintf(__('An account is already registered with your email address. Please %s', 'shoptimizer'), '<a href="'.get_permalink( get_option('woocommerce_myaccount_page_id') ).'" title="'.__('log in','shoptimizer').'>'.__('log in','shoptimizer').'</a>');
    return str_replace($old_message,$new_message,$error);    
}
add_filter( 'woocommerce_add_error', 'my_woocommerce_add_error' );

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}

add_filter( 'woocommerce_available_payment_gateways', 'bbloomer_unset_gateway_by_category' );
  
function bbloomer_unset_gateway_by_category( $available_gateways ) {
    // if ( is_admin() ) return $available_gateways;
    if ( ! is_checkout() ) return $available_gateways;

    global $woocommerce;

    $disable_paypal_for_large_packages = get_field('disable_paypal_for_large_packages', 'option');
    $package_price_limit = get_field('package_price_limit', 'option');

    $cur_user_id = get_current_user_id();
    $disable_paypal_for_large_packages_user = get_field('disable_paypal_for_large_packages', 'user_'.$cur_user_id);
    $package_price_limit_user = get_field('package_price_limit', 'user_'.$cur_user_id);

    $cart_total = $woocommerce->cart->total;
    
    if ($disable_paypal_for_large_packages && $cart_total > $package_price_limit) {
    	if ($disable_paypal_for_large_packages_user) {
    		if ($cart_total > $package_price_limit_user) {
    			unset( $available_gateways['paypal'] );
    		}
    	} else {
    		unset( $available_gateways['paypal'] );
    	}
    } elseif ($disable_paypal_for_large_packages_user && $cart_total > $package_price_limit_user) {
    	unset( $available_gateways['paypal'] );
    }

    return $available_gateways;
}

add_role( 'reseller', 'Reseller', array(
    'read' => true, // True allows that capability
) );

add_action('woocommerce_cart_calculate_fees' , 'add_user_discounts');
/**
 * Add custom fee if more than three article
 * @param WC_Cart $cart
 */
function add_user_discounts( WC_Cart $cart ){

	$user = wp_get_current_user();
 	$roles = ( array ) $user->roles;
	$reseller_discount_perc = get_field('reseller_discount', 'option');

	if ($reseller_discount_perc && in_array('reseller', $roles)) {

		$discount_total = 0;
		$reseller_discount = $reseller_discount_perc / 100;
		$allowed_packages = get_field('allowed_packages', 'option');

	    foreach ($cart->get_cart() as $cart_item) {
	    	if (in_array($cart_item['product_id'], $allowed_packages)) {
		    	$item_total = $cart_item['line_total'];
		    	$discount_total = $discount_total + $item_total;
		    }
	    }

	    if ($discount_total) {
	    	$discount_total = $discount_total * $reseller_discount;
	    	$cart->add_fee( sprintf( __('Reseller discount (%s%%)', 'shoptimizer'), $reseller_discount_perc), - $discount_total);
	    }

	}
}

// Register new status
function register_awaiting_shipment_order_status() {
    register_post_status( 'wc-active', array(
        'label'                     => __('Active', 'shoptimizer'),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Active (%s)', 'Active (%s)' )
    ) );

    register_post_status( 'wc-fraud', array(
        'label'                     => __('Fraud', 'shoptimizer'),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Fraud (%s)', 'Fraud (%s)' )
    ) );

    register_post_status( 'wc-emailed', array(
        'label'                     => __('Emailed', 'shoptimizer'),
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Emailed (%s)', 'Emailed (%s)' )
    ) );
}
add_action( 'init', 'register_awaiting_shipment_order_status' );

// Add to list of WC Order statuses
function add_awaiting_shipment_to_order_statuses( $order_statuses ) {
 
    $order_statuses['wc-active'] = __('Active', 'shoptimizer');
    $order_statuses['wc-fraud'] = __('Fraud', 'shoptimizer');
    $order_statuses['wc-emailed'] = __('Emailed', 'shoptimizer');
 
    return $order_statuses;
}
add_filter( 'wc_order_statuses', 'add_awaiting_shipment_to_order_statuses' );

// Add description to checout company field
function sv_require_wc_company_field( $fields ) {
	$fields['company']['label'] = __('Company (required for VAT number)', 'shoptimizer');
    $fields['company']['description'] = __('Please fill your company name to enable VAT number field', 'shoptimizer');
    return $fields;
}
add_filter( 'woocommerce_default_address_fields', 'sv_require_wc_company_field' );

// Change Empty Cart URL
function wc_empty_cart_redirect_url() {
	return get_home_url() . '/shop';
}
add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url' );


// Login message
add_action( 'woocommerce_before_customer_login_form', 'mv_login_message' );
function mv_login_message() {
    if ( get_option( 'woocommerce_enable_myaccount_registration' ) == 'yes' ) {
	?>
		<div class="woocommerce-info">
			<p><?php _e( 'IMPORTANT: To ensure continuous safety of your MaxVisits account, you will need to update your password as the old one may no longer work after our website\'s recent update. Please use the "Forgot password" option below to create a new password. Thank you!', 'shoptimizer' ); ?></p>
		</div>
	<?php }
}

add_filter('woocommerce_paypal_args', 'custom_paypal_args', 10, 2 );
function custom_paypal_args ( $args, $order) {
    // Saving the data to order meta data (custom field)
    $cart = WC()->cart->get_cart();
    foreach( $cart as $cart_item ){
        $product = wc_get_product( $cart_item['product_id'] );
        $product->get_name();
//        $args['custom'] .= ' Country: ' . WC()->countries->countries[ $order->get_billing_country() ] . ', City: ' . $args['city'] . ', Address: ' . $args['address1'];
        $args['item_name_1'] = 'MV - ' . $product->get_name() . ' - Order #' . $order->get_order_number() /* . 'Invoice #' . $invoice->invoice_number()*/;
        break;
    }
    update_post_meta( $order->get_id(), '_test_paypal', $args );
    return $args;
}

function is_valid_url_check($url) {
    $url = @parse_url($url);
    if (!$url) {
        return false;
    }
    $url = array_map('trim', $url);
    $url['port'] = (!isset($url['port'])) ? 80 : (int)$url['port'];
    $path = (isset($url['path'])) ? $url['path'] : '';
    if ($path == '') {
        $path = '/';
    }
    $path .= (isset($url['query'])) ?  "?$url[query] " : '';
    if (isset($url['host']) AND $url['host'] != gethostbyname($url['host'])) {
        if (PHP_VERSION  >= 5) {
            $headers = get_headers( "$url[scheme]://$url[host]$path ");
        } else {
            $fp = fsockopen($url['host'], $url['port'], $errno, $errstr, 30);
            if (!$fp) {
                return false;
            }
            fputs($fp,  "HEAD $path HTTP/1.1\r\nHost: $url[host]\r\n\r\n ");
            $headers = fread($fp, 4096);
            fclose($fp);
        }
        $headers = (is_array($headers)) ? implode( "\n ", $headers) : $headers;
        return (bool)preg_match('#^HTTP/.*\s+[(200|301|302)]+\s#i', $headers);
    }
    return false;
}

function add_the_url_validation( $passed ) {
	if ( isset($_REQUEST['ppom']['fields']['url']) && !empty($_REQUEST['ppom']['fields']['url']) ) {
	    if ( !is_valid_url_check($_REQUEST['ppom']['fields']['url']) ) {
		    wc_add_notice( __( 'Please enter valid URL', 'woocommerce' ), 'error' );
		    $passed = false;
		}
	} else {
		$passed = false;
	}
	
	return $passed;
}
// add_filter( 'woocommerce_add_to_cart_validation', 'add_the_url_validation', 99, 1 ); 

add_filter( 'manage_edit-shop_order_columns', 'add_payment_method_column', 20 );
function add_payment_method_column( $columns ) {
	$new_columns = array();
	foreach ( $columns as $column_name => $column_info ) {
		$new_columns[ $column_name ] = $column_info;
		if ( 'order_total' === $column_name ) {
			$new_columns['order_payment'] = __( 'Payment Method', 'woocommerce' );
		}
	}
	return $new_columns;
}

add_action( 'manage_shop_order_posts_custom_column', 'add_payment_method_column_content' );
function add_payment_method_column_content( $column ) {
	global $post;
	if ( 'order_payment' === $column ) {
		$order = wc_get_order( $post->ID );
		echo $order->get_payment_method_title();
	}
}

add_filter( 'woocommerce_bacs_account_fields', 'custom_bacs_account_field', 10, 2);
function custom_bacs_account_field( $account_fields, $order_id ) {

	$bank_name_val = '<br>' . str_replace( array( '|', '*b/', '*b' ) , array('<br>', '</b>', '<b>'), $account_fields['bank_name']['value'] );
	$account_fields = [];

    $account_fields['bank_name'] = array(
        'label' => __( 'Bank Information', 'woocommerce' ),
        'value' => $bank_name_val
    );

    return $account_fields;

}

// AJAX

add_action('wp_ajax_try_for_free', 'try_for_free_ajax');
add_action('wp_ajax_nopriv_try_for_free', 'try_for_free_ajax');

function try_for_free_ajax() {
	echo '<div id="freetrialpopupCont">
            <div id="freetrialBox">
            	<div id="container-trial">
					<h1>'.__('Maxvisits Free Trial', 'shoptimizer').'</h1>
						<p>'.__('If you\'d like to test our traffic before committing any money, please', 'shoptimizer').' <a target="_blank" title="'.__('Get in touch with us', 'shoptimizer').'" href="/contact.html">'.__('get in touch with us', 'shoptimizer').'</a> '. __('and we\'ll get some free traffic heading your way!', 'shoptimizer').'</p>
						<p>'.__('(Please keep in mind that our rules still apply, so your target URL can\'t break any of them - please see', 'shoptimizer').' <a target="_blank" title="'.__('Terms of Service', 'shoptimizer').'" href="/tos.html">'.__('Terms of Service', 'shoptimizer').'</a> '.__('for more details)', 'shoptimizer').'</p>
				</div>
				<a href="#" class="nyroModalCloseButton closeX closeTrial" title="'.__('Close', 'shoptimizer').'">×</a>
			</div>
        </div>';

	wp_die();
}

// Payment methods config

add_filter( 'woocommerce_available_payment_gateways', 'rebel_payment_gateway_disable_country' );
function rebel_payment_gateway_disable_country( $available_gateways ) {

	if (is_checkout()) {
		$currenties_list = array('USD', 'EUR');
		$curent_currency = get_woocommerce_currency();
	    if ( !in_array($curent_currency, $currenties_list) ) {
	        unset( $available_gateways['bacs'] );
	    }
	}
	
	return $available_gateways;
}