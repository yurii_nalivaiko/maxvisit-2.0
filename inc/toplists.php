<?php

namespace Rebel\SuperScript\Themes\MrKortingscode;

// Enable thumbnails support.
add_post_type_support('toplist', 'thumbnail');

// Add theme settings metabox to toplist edit screen.
add_action('acf/register_fields', function () {
    register_field_group(array(
        'id' => 'acf_mrk-toplists',
        'title' => 'Theme Settings',
        'fields' => array(
            array(
                'key' => 'field_5a8d481430615',
                'label' => 'Locale',
                'name' => 'locale_override',
                'type' => 'select',
                'choices' => apply_filters('mrk/available_langs', array()),
                'default_value' => get_locale(),
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array(
                'key' => 'field_5aafd2335a1d2',
                'label' => 'Keyword',
                'name' => 'meta_keyword',
                'type' => 'text',
                'instructions' => 'To be used with <code>[keyword]</code> shortcode in page meta.',
                'formatting' => 'none',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'toplist',
                    'order_no' => 0,
                    'group_no' => 0
                )
            )
        ),
        'options' => array(
            'position' => 'side',
            'layout' => 'default',
            'hide_on_screen' => array()
        ),
        'menu_order' => 1
    ));
});

// Append toplist output to the content.
add_filter('the_content', function ($content) {
    if (is_singular('toplist')) {
        if (is_page_template('toplist-template-index.php')) {
            $part = 'toplist-idx';
        } else {
            $part = 'toplist';
        }
        
        ob_start();
        
        while (have_rows('websites')) {
            the_row();
            wc_get_template_part($part, 'item');
        }
        
        $content .= ob_get_clean();
    }
    
    return $content;
});

// Print linked website name.
add_filter('toplists/acf/get_website_fields', function (array $fields) {
    $fields[] = array(
        'key' => 'field_5d5d4f070f0ed',
        'label' => 'No follow',
        'name' => 'mrk_rel_follow',
        'type' => 'true_false',
        'message' => 'Remove <code>rel=nofollow</code> attribute from link',
        'default_value' => 0,
    );
    
    return $fields;
});

add_action('mrk/toplists/print_website_name', function () {
    $url    = get_sub_field('url');
    $domain = preg_replace('/^www\./i', '', parse_url($url, PHP_URL_HOST));
    $name   = get_sub_field('name') ?: $domain;
    $rel    = get_sub_field('mrk_rel_follow') ? '' : ' rel="nofollow noopener noreferrer"';
    
    printf(
        '<a href="%s" target="_blank"%s>%s</a>',
        esc_attr($url),
        $rel,
        esc_html($name)
    );
});

// Print website logo.
add_action('mrk/toplists/print_website_logo', function () {
    $logo = get_sub_field('logo');
    
    if (isset($logo['url'], $logo['alt'])) {
        printf(
            '<img src="%s" class="img-responsive" alt="%s">',
            esc_attr($logo['url']),
            esc_attr($logo['alt'])
        );
    }
});

// Print rating stars.
add_action('mrk/toplists/print_website_rating', function ($rating) {
    $rating = get_sub_field($rating . '_rating');
    
    if (is_numeric($rating)) {
        printf(
            '<span class="rating-highlight">%s</span>%s',
            str_repeat('★', $rating),
            str_repeat('☆', 5 - $rating)
        );
    }
});

// Return website index for "Index" template.
add_filter('mrk/toplists/idx/get_index', function () {
    ob_start();
    
    while (have_rows('websites')) {
        the_row();
        do_action('mrk/toplists/print_website_name');
    }
    
    reset_rows();
    
    return ob_get_clean();
});

// Append index to the toplist's content in "Index" template.
add_filter('the_content', function ($content) {
    if (is_singular('toplist') && is_page_template('toplist-template-index.php')) {
        $content = sprintf(
            '<div class="mrk-intro">' . // Concatenate lines for compatibility
                '%s' .                  // with WP's stupid autop filter.
                '<div class="mrk-index">' .
                    '<h6 class="mrk-header">%s:</h6>' .
                    '<div>%s</div>' .
                '</div>' .
            '</div>',
            wpautop($content),
            _x('Index', 'toplists', 'mrk'),
            apply_filters('mrk/toplists/idx/get_index', '')
        );
    }
    
    return $content;
}, 5);

// Add logo badge field for "Default" template.
add_filter('toplists/acf/get_website_fields', function (array $fields) {
    $fields[] = array(
        'key' => 'field_5e95c154ef125',
        'label' => 'Logo badge',
        'name' => 'logo_badge',
        'type' => 'image',
        'instructions' => 'Works with <em>Default</em> template only.',
        'save_format' => 'object',
        'preview_size' => 'medium',
        'library' => 'all',
    );
    
    return $fields;
});

// Add badge field for "Index" template.
add_filter('toplists/acf/get_website_fields', function (array $fields) {
    $fields[] = array(
        'key' => 'field_5ae03d650cb62',
        'label' => 'Badge image',
        'name' => 'badge_image',
        'type' => 'image',
        'instructions' => 'Works with <em>Index</em> template only.',
        'save_format' => 'object',
        'preview_size' => 'medium',
        'library' => 'all',
    );
    
    return $fields;
});

// Print logo badge in "Default" template.
add_action('mrk/toplists/print_logo_badge', function () {
    $badge = get_sub_field('logo_badge');
    
    if (isset($badge['url'])) {
        printf('<img src="%s" class="site-logo-badge" alt="">', esc_attr($badge['url']));
    }
});

// Print badge image in "Index" template.
add_action('mrk/toplists/idx/print_badge', function () {
    $badge = get_sub_field('badge_image');
    
    if (isset($badge['url'])) {
        printf('<img src="%s" alt="">', esc_attr($badge['url']));
    }
});

// Return badge code for "Index" template.
add_filter('mrk/toplists/idx/get_code', function ($code, $width) {
    $badge = get_sub_field('badge_image');
    
    if (isset($badge['url'])) {
        $code = sprintf(
            '<a href="%s"><img src="%s" alt="%s" style="width: %dpx;"></a>',
            esc_attr(get_permalink()),
            esc_attr($badge['url']),
            esc_attr(get_the_title()),
            $width
        );
    }
    
    return $code;
}, 10, 2);
