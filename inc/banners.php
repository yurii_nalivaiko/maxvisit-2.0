<?php

namespace Rebel\SuperScript\Themes\MrKortingscode;

// Add theme settings metabox to edit screen.
if ( function_exists('acf_add_local_field_group') ) {
    acf_add_local_field_group(array(
        'id' => 'acf_mrk-banners',
        'title' => 'Theme Settings',
        'fields' => array(
            array (
                'key' => 'field_58d11e91d43b3',
                'label' => 'Header background',
                'name' => 'header_background',
                'type' => 'image',
                'save_format' => 'object',
                'preview_size' => 'medium',
                'library' => 'all',
            ),
            array (
                'key' => 'field_58d3919e95e73',
                'label' => 'URL',
                'name' => 'url',
                'type' => 'text',
                'required' => 1,
                'default_value' => home_url(),
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_593130070c891',
                'label' => 'Banners in row',
                'name' => 'banners_in_row',
                'type' => 'radio',
                'choices' => array(
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    6 => 6,
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 1,
                'layout' => 'horizontal',
            ),
            array (
                'key' => 'field_593fcb1d38def',
                'label' => 'Help Popup',
                'name' => 'help_popup',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'basic',
                'media_upload' => 'no',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'banner_page',
                    'order_no' => 0,
                    'group_no' => 0
                )
            )
        ),
        'options' => array(
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array()
        ),
        'menu_order' => 0
    ));
}

// Return header background image's URL.
add_filter('mrk/banners/get_header_background', function ($url) {
    $bg = get_field('header_background');
    return isset($bg['url']) ? $bg['url'] : $url;
});

// Return banner code to copy.
add_filter('mrk/banners/get_code', function () {
    $image  = get_sub_field('image');
    $hidden = get_sub_field('hidden_html');
    
    if ($hidden == '') {
        // Fallback to linked banner.
        return sprintf(
            '<a href="%1$s" title="%2$s"><img src="%3$s" alt="%2$s"></a>',
            esc_attr(get_field('url')),
            esc_attr(get_the_title()),
            esc_attr($image['url'])
        );
    }
    
    // Render banner as background and hide extra content below.
    return sprintf(
        '<div style="background:url(%s);width:%spx;height:0;padding-top:%spx;overflow:hidden;">%s</div>',
        esc_attr($image['url']),
        esc_attr($image['width']),
        esc_attr($image['height']),
        $hidden
    );
});

// Calculate banners grid.
add_filter('mrk/banners/calc_cols', function (array $columns, $count, $maxInRow) {
    $inRow = min(array($count, $maxInRow));
    
    switch ($inRow) {
        case 0: case 1:
            return array('span' => 12, 'offset' => 0);
        case 2:
            return array('span' => 6, 'offset' => 0);
        case 3:
            return array('span' => 4, 'offset' => 0);
        case 4:
            return array('span' => 3, 'offset' => 0);
        case 5:
            return array('span' => 2, 'offset' => 1);
        default:
            return array('span' => 2, 'offset' => 0);
    }
}, 10, 3);

// Determine layout and render banners grid.
add_action('mrk/banners/render_grid', function () {
    $maxInRow = get_field('banners_in_row');
    $layout   = ($maxInRow > 1) ? 'horizontal' : 'vertical';
    
    wc_get_template_part('banners', $layout);
});

// Greenbox shortcode.
add_shortcode('bp-greenbox', function ($atts, $content = '') {
    return sprintf(
        '<div class="bp-greenbox">%s</div>',
        do_shortcode($content)
    );
});

// Donation counter shortcode.
add_shortcode('bp-donated', function ($atts, $content = '') {
    // str_split() does not support multi-byte strings.
    $chars = preg_split('//u', $content, null, PREG_SPLIT_NO_EMPTY);
    
    return sprintf(
       '<span class="bp-highlight">
            %s:
            <span class="bp-counter"><span>%s</span></span>
        </span>',
        _x('Donated so far', 'banners', 'mrk'),
        implode('</span><span>', $chars)
    );
});
