<?php

namespace Rebel\SuperScript\Themes\MrKortingscode;

get_header();
the_post();

?>
<h2 class="bp-header"><?php the_title(); ?></h2>
<div class="container bp-banners">
    <?php $content = explode('[banners]', get_the_content(), 2); ?>
    <div class="row">
        <div class="col-sm-offset-1 col-sm-10 instructions">
            <?= apply_filters('the_content', $content[0]) ?>
        </div>
    </div>
    <?php do_action('mrk/banners/render_grid'); ?>
    <?php if (isset($content[1])): ?>
    <div class="row">
        <div class="col-sm-offset-1 col-sm-10 instructions">
            <?= apply_filters('the_content', $content[1]) ?>
        </div>
    </div>
    <?php endif; ?>
</div>
<style>
    .bp-header {
        background-image: url('<?= esc_js(
            apply_filters('mrk/banners/get_header_background', '#')
        ) ?>');
    }
    
    .popover {
        width: 400px;
        max-width: 400px;
    }
    
    .popover .popover-title {
        font-weight: bold;
        text-align: center;
    }
</style>
<script src="//cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="//cdn.jsdelivr.net/clipboard.js/1.6.1/clipboard.min.js"></script>
<script>
    jQuery(function ($) {
        new Clipboard('.single-banner .button', {
            target: function(trigger) {
                return $(trigger).prev('textarea').get(0);
            }
        });
        
        <?php if (get_field('help_popup')): ?>
        $('.single-banner .image').popover({
            title: '<?= esc_js(__('What to do?', 'mrk')) ?>',
            content: function () {
                return $('#help-popup').html();
            },
            html: true,
            placement: function (popover, banner) {
                var row        = $(banner).closest('.row');
                var isVertical = $(row).hasClass('banners-vertical');
                return isVertical ? 'auto left' : 'top';
            },
            trigger: 'hover'
        });
        <?php endif; ?>
    });
</script>
<script id="help-popup" type="text/html">
    <?php the_field('help_popup'); ?>
</script>
<?php

get_footer();
