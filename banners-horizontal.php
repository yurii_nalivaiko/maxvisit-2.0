<?php

namespace Rebel\SuperScript\Themes\MrKortingscode;

$banners  = get_field('banners') ?: array();
$count    = count($banners);
$maxInRow = get_field('banners_in_row');
$columns  = array('span' => 2, 'offset' => 0);
$columns  = apply_filters('mrk/banners/calc_cols', $columns, $count, $maxInRow);
$col      = 0;

?>
<div class="row banners-horizontal">
    <?php while (have_rows('banners')): the_row(); ?>
    <?php if ($col++ % $maxInRow == 0): ?>
    <div class="col-sm-<?= $columns['span'] ?> col-sm-offset-<?= $columns['offset'] ?> bp-row-break single-banner">
    <?php else: ?>
    <div class="col-sm-<?= $columns['span'] ?> single-banner">
    <?php endif; ?>
        <?php $image = get_sub_field('image'); ?>
        <?php $title = get_sub_field('title'); ?>
        <div class="image">
            <img class="img-responsive" src="<?= esc_attr($image['url']) ?>"
                 alt="<?= esc_attr($title ?: get_the_title()) ?>">
        </div>
        <div class="embed-code">
            <strong><?=
                $title ? do_shortcode(esc_html($title)) : sprintf('%dx%d', $image['width'], $image['height'])
          ?></strong>
            <textarea cols="5" rows="5" onclick="this.select()"><?= esc_html(
                apply_filters('mrk/banners/get_code', '', array())
            ) ?></textarea>
            <input class="button button-primary" type="button" 
                   value="<?= esc_attr(__('Copy banner code', 'mrk')) ?>">
        </div>
    </div>
    <?php endwhile; ?>
</div>
