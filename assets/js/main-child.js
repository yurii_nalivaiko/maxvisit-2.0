// Main Shoptimizer js.
( function( $ ) {
	'use strict';

	// Change URL type 
	// $('.single-product #url').attr('type', 'url');
	// $('.single-product #url').after( '<a href="#" id="check_compatibility">Test my URL</active>' );
	$('.ppom-quantity').attr('min', '40').attr('max', '15000');

	function isUrlValid(url) {
	    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
	}

	$('#check_compatibility').on('click', function(event) {
		event.preventDefault();

		if ($('.single-product #url').val() != undefined && $('.single-product #url').val() != '') {

			var url = $('.single-product #url').val();
			var src = '/checkurl.php?url='+url;

			if ( isUrlValid(url) ) {
				window.open(src, '_blank');
				$('.ppom-col .url .urlerrormsg').remove();
			} else {
				$('#url').focus();
				if (!$('.ppom-col .url .urlerrormsg').length) {
					$('.ppom-input-url').after('<span class="urlerrormsg">' + $('.single-product #url').attr('data-errormsg') + '</span>');
				}
			}

		} else {
			$('#url').focus();
			if (!$('.ppom-col .url .urlerrormsg').length) {
				$('.ppom-input-url').after('<span class="urlerrormsg">' + $('.single-product #url').attr('data-errormsg') + '</span>');
			}
		}

	});

	$('.topbar-wrapper .menu-toggle-custom').on('click', function(event) {
		event.preventDefault();

		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			$('.topbar-wrapper .top-bar').hide();
			$('.custom-logo').show();
		} else {
			$(this).addClass('active');
			$('.topbar-wrapper .top-bar').show();
			$('.custom-logo').hide();
		}
		
	});

	var try_popup_opened = 0;
	$('.try_for_free').on('click', function (event){
		event.preventDefault();
		if (!try_popup_opened) {
			try_popup_opened = 1;
			var data = {
				action: 'try_for_free',
			};

			jQuery.post( woocommerce_params.ajax_url, data, function(response) {
				if (response) {
					$('body').append(response);
					activate_free_script();
				}
			});
		}
	});

	function activate_free_script() {

		$('#freetrialBox .nyroModalCloseButton').on('click', function(event) {
			event.preventDefault();
			$('#freetrialpopupCont').hide();
			$('#freetrialpopupCont').remove();
			try_popup_opened = 0;
		});

	}

	if (window.location.hash) {
		var hash_id = window.location.hash;
		if ($(hash_id).length) {
			$([document.documentElement, document.body]).animate({
				scrollTop: $(hash_id).offset().top-50
			}, 1500);

			setTimeout(function(){
				$(hash_id).click();
			}, 2000);
			
		}
	}

	// Reseller discount price change
	if ($('#reseller_discount_perc').length) {
		$('.volume input').each( function(){
			var price = $(this).attr('data-price');
			var discount = $('#reseller_discount_perc').val();
			price = price - (price * (discount/100));
			$(this).attr('data-price', price);
		});
	}
	
}( jQuery ) );
