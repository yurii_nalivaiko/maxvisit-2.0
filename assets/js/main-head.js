// Main Head Shoptimizer js.
( function( $ ) {
	'use strict';

	$("body:not(.reseller-user) .ppom-wrapper .volume input[data-optionid='5_000_visits']").parents('.form-check').remove();
	$("body:not(.reseller-user) .ppom-wrapper #delivery_speed option[data-optionid='60_days']").remove();
	
}( jQuery ) );
