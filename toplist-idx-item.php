<?php namespace Rebel\SuperScript\Themes\MrKortingscode; ?>
<div class="mrk-toplist-website">
    <div class="row">
        <div class="mrk-badge col-sm-6">
            <?php do_action('mrk/toplists/idx/print_badge'); ?>
        </div>
        <div class="mrk-code col-sm-6">
            <textarea><?= esc_textarea(apply_filters('mrk/toplists/idx/get_code', '', 200)) ?></textarea>
            <select>
                <option value="200">200px</option>
                <option value="300">300px</option>
                <option value="400">400px</option>
            </select><?php // Remove white space.
          ?><input type="button" value="<?= esc_attr(_x('Copy code', 'toplists', 'mrk')) ?>">
        </div>
    </div>
    <div class="row">
        <div class="mrk-logo col-sm-4 text-center">
            <?php do_action('mrk/toplists/print_website_logo'); ?>
            <a href="<?= esc_attr(get_sub_field('url')) ?>" target="_blank">
                <?= _x('Visit site', 'toplists', 'mrk') ?> &rsaquo;
            </a>
        </div>
        <div class="mrk-desc col-sm-8">
            <?php the_sub_field('description'); ?>
        </div>
    </div>
</div>