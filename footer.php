<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Shoptimizer
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->


	<?php do_action( 'shoptimizer_before_footer' ); ?>

	<?php
	/**
	 * Functions hooked in to shoptimizer_footer action
	 */
	do_action( 'shoptimizer_footer' );
	?>

	<?php do_action( 'shoptimizer_after_footer' ); ?>


</div><!-- #page -->

<?php wp_footer(); ?>

</body>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','https://connect.facebook.net/en_US/fbevents.js');

  fbq('init', '551089208804794');
  fbq('track', "PageView");
</script>

<script type="text/javascript">
	if (typeof success_order_total != 'undefined') {
		fbq('track', 'Purchase', { value: success_order_total, currency: 'USD' });
	}
</script>

<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=551089208804794&ev=PageView&noscript=1"
  /></noscript>
<!-- End Facebook Pixel Code -->

</html>
